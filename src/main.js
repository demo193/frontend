import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import router from './router'
import vuetify from './plugins/vuetify';
import "./assets/styles.scss";


Vue.config.productionTip = false

Vue.use(BootstrapVue)

new Vue({
  router,
  vuetify,
  render: h => h(App),
}).$mount('#app')
