import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "login",
      component: () => import("./components/Login"),
    },
    {
      path: "/logs",
      name: "logs",
      component: () => import("./components/LogsList")
    },
    {
      path: "/logsDescription",
      name: "logDescription",
      component: () => import("./components/LogDescription")
    },
    {
      path: "/customers",
      name: "customers",
      component: () => import("./components/Customer")
    }
  ]
});
