import http from "../http-common";

class CustomerDataService {
  getAll(params) {
    return http.get("/customers", { params });
  }

  get(userid) {
    return http.get(`/customers/${userid}`);
  }

  findByTitle(userid) {
    return http.get(`/customers?user=${userid}`);
  }
}

export default new CustomerDataService();
